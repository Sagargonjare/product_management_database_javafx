package showOptions;

import addProducts.AddProduct;
import deleteProduct.DeleteProduct;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import searhProduct.SearchProduct;
import updateProduct.UpdateProduct;



public class ShowOptionsController {
	@FXML
	private Button add;
	@FXML
	private Button update;
	@FXML
	private Button search;
	@FXML
	private Button delete;
	@FXML
	private Button exit;
	public void add(ActionEvent event) {
		new AddProduct().show();
		
	}
	public void update(ActionEvent event) {
		new UpdateProduct().show();
	}
	public void delete(ActionEvent event) {
		new DeleteProduct().show();
	}
	public void search(ActionEvent event ) {
		new SearchProduct();
	}
	public void exit(ActionEvent event) {
		System.out.println("Exit application");
	}
	
	
	

}